# Build and Test Pipeline

This repository hosts a gitlab-ci pipeline for a Linux kernel repository.

This is to make it easier to run CI (build, boot and run functional tests) on a
given Linux kernel repository.

Upstream projects that is used are
[TuxBuild](https://gitlab.com/Linaro/tuxbuild/blob/master/README.md) and
[SQUAD](https://qa-reports.linaro.org/).  SQUAD sends test jobs to
[LAVA](https://lkft.validation.linaro.org/) that schedules the jobs on the
different devices.

## Setup TuxBuild

### Create a gitlab mirror

First, [create a new gitlab mirror of a kernel
repository](https://docs.gitlab.com/ee/user/project/repository/repository_mirroring.html).

### Enable gitlab-ci

Once the mirror exists, navigate to `Settings -> CI/CD -> Custom CI
configuration path` and enter the url to [gitlab-ci.yml](gitlab-ci.yml). e.g.
`https://gitlab.com/Linaro/lkft/users/anders.roxell/linux-pipeline/-/raw/master/gitlab-ci.yml`.

### Add a TuxBuild Token to kernel-runs

The mirror repository will also need a TUXBUILD_TOKEN set in its CI
environment. Navigate to `Settings->CI/CD->Variables`, set the Key name to
"TUXBUILD_TOKEN" and the Value to your tuxbuild token. Select "Masked" and
click "Save variables".

If you don't have a valid TuxBuild token, follow the instructions from
[TuxBuild, using the
api](https://gitlab.com/Linaro/tuxbuild/blob/master/README.md#using-the-api) in
order to ask for one.

### Increase timeout

If a job can take more than one hour, go to `CI/CD->General` pipelines and
increate the Timeout to 2h or 3h, depending on your max runtime expected.

### Remove Protected Branches

When gitlab creates a new repository, it will by default protect the 'master'
branch, which prevents force pushes. In the event that a 'master' branch is
being mirrored, this may prevent updates to the master branch from the upstream
mirror.

To remove, navigate to `Settings->Repository->Protected Branches`, click
Expand, and click the yellow "Unprotect" button.

## Setup SQUAD

### Enable gitlab-ci

Once the mirror exists, navigate to `Settings -> CI/CD -> Custom CI
configuration path` and enter the url to [gitlab-ci.yml](gitlab-ci.yml). e.g.
`https://gitlab.com/Linaro/lkft/users/anders.roxell/linux-pipeline/-/raw/master/gitlab-ci.yml`.

### Add a SQUAD Token to submit sanity and test jobs

The mirror repository will also need a QA_REPORTS_TOKEN set in its CI
environment. Navigate to `Settings->CI/CD->Variables`, set the Key name to
"QA_REPORTS_TOKEN" and the Value to your SQUAD token. Select "Masked" and
click "Save variables".

If you don't have a [SQUAD account](https://qa-reports.linaro.org), email
<lkft@linaro.org> to get login credentials to
[SQUAD](https://qa-reports.linaro.org) and then create a
[token](https://qa-reports.linaro.org/_/settings/api-token/) to use.

When you are logged into SQUAD you need to create your own ["user
namespace"](https://qa-reports.linaro.org/_/settings/projects/).
Then you have to add that to the 'QA_TEAM variable in gitlab-ci.yml and that
will be '~firstname.lastname'.

# Usage

## Only Building

Add a tuxconfig.yml file, and in gitlab-ci.yml connect them with the 'build_set_name'
Under the '.create' step we specify which branches we want to build.

# Dependencies
Linaro/lkft/pipelines/common that needs to be included into gitlab-ci.yml file
and it depends on a tuxconfig.yml file too.
